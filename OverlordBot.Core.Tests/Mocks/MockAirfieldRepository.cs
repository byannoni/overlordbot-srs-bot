﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.Threading.Tasks;
using Geo.Geometries;
using RurouniJones.OverlordBot.Datastore;
using Unit = RurouniJones.OverlordBot.Datastore.Models.Unit;

#pragma warning disable 1998

namespace RurouniJones.OverlordBot.Core.Tests.Mocks
{
    internal class MockAirfieldRepository : IAirfieldRepository
    {
        public List<Unit> GetAllLandAirfieldsResult { get; set; } = new();
        public Unit FindByNameResult{ get; set; } = null;
        public Unit FindByClosestResult{ get; set; } = null;

        public async Task<IEnumerable<Unit>> GetAllLandAirfields(string serverShortName)
        {
            return GetAllLandAirfieldsResult;
        }

        public async Task<Unit> FindByName(string name, string serverShortName)
        {
            return FindByNameResult;
        }

        public async Task<Unit> FindByClosest(Point sourcePosition, int coalition, string serverShortName)
        {
            return FindByClosestResult;
        }
    }
}

﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Atc;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Datastore.Models;
using RurouniJones.OverlordBot.Encyclopedia;

namespace RurouniJones.OverlordBot.Core.Tests.AtcLogic
{
    [TestClass]
    public class AtcStateMachineTests
    {
        #pragma warning disable 1998
        public static void SendTransmissionMock(Reply result) {}
        public static async Task LogTransmissionMock(string text) {}
        #pragma warning restore 1998

        private static ControlledAircraft GetPlaneA(Core.AirfieldStatus airfield, List<Airfield.NavigationPoint> navigationPoints )
        {
            return new ControlledAircraft(new ITransmission.Player("Dolt", 1, 1),
                new Unit { Id = "planeA" }, airfield, navigationPoints, SendTransmissionMock, LogTransmissionMock);
        }

        private static ControlledAircraft GetPlaneB(Core.AirfieldStatus airfield, List<Airfield.NavigationPoint> navigationPoints )
        {
            return new ControlledAircraft(new ITransmission.Player("Dolt", 1, 2),
                new Unit { Id = "planeB" }, airfield, navigationPoints, SendTransmissionMock, LogTransmissionMock);
        }

        private Core.AirfieldStatus _airfield;

        [TestInitialize]
        public void Setup()
        {
            _airfield = new Core.AirfieldStatus(Airfield.Entries.First(a => a.Name == "Anapa-Vityazevo"), "TEST");
        }

        [TestMethod]
        public async Task HoldingShort_WhenOtherAircraftIsLinedUpAndWaiting_ThenHoldShort()
        {
            var navigationPoints = new List<Airfield.NavigationPoint> {_airfield.Runways.First()};

            var planeA = GetPlaneA(_airfield, navigationPoints);
            var planeB = GetPlaneB(_airfield, navigationPoints);

            planeA.SetInitialState(ControlledAircraft.State.LinedUpAndWaiting);
            planeB.SetInitialState(ControlledAircraft.State.HoldingShort);

            _airfield.ControlledPlayers[planeA.PlayerUnit.Id] = planeA;
            _airfield.ControlledPlayers[planeB.PlayerUnit.Id] = planeB;

            await planeB.CheckHoldingShort();

            Assert.IsTrue(planeB.AtcState.IsInState(ControlledAircraft.State.HoldingShort));
        }

        [TestMethod]
        public async Task HoldingShort_WhenOtherAircraftIsClearedTakeoff_ThenHoldShort()
        {
            var navigationPoints = new List<Airfield.NavigationPoint> {_airfield.Runways.First()};

            var planeA = GetPlaneA(_airfield, navigationPoints);
            var planeB = GetPlaneB(_airfield, navigationPoints);

            planeA.SetInitialState(ControlledAircraft.State.ClearedForTakeoff);
            planeB.SetInitialState(ControlledAircraft.State.HoldingShort);

            _airfield.ControlledPlayers[planeA.PlayerUnit.Id] = planeA;
            _airfield.ControlledPlayers[planeB.PlayerUnit.Id] = planeB;

            await planeB.CheckHoldingShort();

            Assert.IsTrue(planeB.AtcState.IsInState(ControlledAircraft.State.HoldingShort));
        }

        [TestMethod]
        public async Task HoldingShortToTakeRunway_WhenOtherAircraftIsRolling_ThenLineUpAndWait()
        {
            var navigationPoints = new List<Airfield.NavigationPoint> {_airfield.Runways.First()};

            var planeA = GetPlaneA(_airfield, navigationPoints);
            var planeB = GetPlaneB(_airfield, navigationPoints);

            planeA.SetInitialState(ControlledAircraft.State.Rolling);
            planeB.SetInitialState(ControlledAircraft.State.HoldingShort);

            _airfield.ControlledPlayers[planeA.PlayerUnit.Id] = planeA;
            _airfield.ControlledPlayers[planeB.PlayerUnit.Id] = planeB;

            await planeB.CheckHoldingShort();

            Assert.IsTrue(planeB.AtcState.IsInState(ControlledAircraft.State.LinedUpAndWaiting));
        }

        [TestMethod]
        public async Task HoldingShortToCrossRunway_WhenOtherAircraftIsRolling_ThenContinueToHoldShort()
        {
            var navigationPointsA = new List<Airfield.NavigationPoint> {_airfield.Runways.First()};
            var navigationPointsB = new List<Airfield.NavigationPoint> { _airfield.Runways.Last(), _airfield.Runways.First()};


            var planeA = GetPlaneA(_airfield, navigationPointsA);
            var planeB = GetPlaneB(_airfield, navigationPointsB);

            _airfield.ControlledPlayers[planeA.PlayerUnit.Id] = planeA;
            _airfield.ControlledPlayers[planeB.PlayerUnit.Id] = planeB;

            planeA.SetInitialState(ControlledAircraft.State.Rolling);
            planeB.SetInitialState(ControlledAircraft.State.HoldingShort);

            _airfield.ControlledPlayers[planeA.PlayerUnit.Id] = planeA;
            _airfield.ControlledPlayers[planeB.PlayerUnit.Id] = planeB;

            await planeB.CheckHoldingShort();

            Assert.IsTrue(planeB.AtcState.IsInState(ControlledAircraft.State.HoldingShort));
        }

        [TestMethod]
        public async Task HoldingShortToCrossRunway_WhenOtherAircraftIsLandedButStillOnRunway_ThenContinueToHoldShort()
        {
            var navigationPointsA = new List<Airfield.NavigationPoint> {_airfield.Runways.First()};
            var navigationPointsB = new List<Airfield.NavigationPoint> { _airfield.Runways.Last(), _airfield.Runways.First()};


            var planeA = GetPlaneA(_airfield, navigationPointsA);
            var planeB = GetPlaneB(_airfield, navigationPointsB);

            _airfield.ControlledPlayers[planeA.PlayerUnit.Id] = planeA;
            _airfield.ControlledPlayers[planeB.PlayerUnit.Id] = planeB;

            planeA.SetInitialState(ControlledAircraft.State.Landed);
            planeB.SetInitialState(ControlledAircraft.State.HoldingShort);

            _airfield.ControlledPlayers[planeA.PlayerUnit.Id] = planeA;
            _airfield.ControlledPlayers[planeB.PlayerUnit.Id] = planeB;

            await planeB.CheckHoldingShort();

            Assert.IsTrue(planeB.AtcState.IsInState(ControlledAircraft.State.HoldingShort));
        }
    }
}

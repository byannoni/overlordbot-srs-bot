﻿/* 
Custodian is a DCS server administration tool for Discord
Copyright (C) 2022 Jeffrey Jones

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using Microsoft.Extensions.DependencyInjection;
using RurouniJones.Dcs.Grpc.V0.Coalition;
using RurouniJones.Dcs.Grpc.V0.Trigger;
using RurouniJones.OverlordBot.Datastore;

namespace RurouniJones.TextalordBot.Console
{
    public class MonitorFactory
    {
        private readonly IServiceProvider _serviceProvider;
        public MonitorFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public Monitor CreateMonitor(
            IPlayerRepository playerRepository,
            IUnitRepository unitRepository,
            CoalitionService.CoalitionServiceClient coalitionClient,
            TriggerService.TriggerServiceClient triggerClient
            )
        {
            return ActivatorUtilities.CreateInstance<Monitor>(_serviceProvider,
                playerRepository, unitRepository, coalitionClient, triggerClient);

        }
    }
}

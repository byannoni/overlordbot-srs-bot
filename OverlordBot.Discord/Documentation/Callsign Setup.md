﻿**Callsign setup**
If you prefer to watch a youtube tutorial, see: https://www.youtube.com/watch?v=ELmi_VitwQg&feature=youtu.be
Callsign format is `GROUPNAME X-X` where `GROUPNAME` is a word and X-X are two numbers, `FLIGHT` and `ELEMENT`.

**Callsign restrictions**
* `GROUPNAME` should be a common english word that is easily understandable over the radio. It can be uppercase, lowercase or mixed-case. All upper-case is recommended.
* `GROUPNAME` _cannot_ be a word used in the NATO phonetic alphabet (`Alpha`, `Bravo` etc) and avoid words already reserved for brevity usage.
* `FLIGHT` number must be a number between between `1-9` inclusive.");
* `ELEMENT` number must be a number between `1-99` inclusive.
* Avoid `2` and `4` in the callsign numbers if possible as they have a higher recognition error rate. If you cannot then submit a voice training set for best results.

**Setting your callsign**
* Set your callsign in the multiplayer server browser. You can still keep your existing nickname. The de-facto formatting is `GROUPNAME X-X | Nickname`.
* Put your callsign first and nickname last to make it easier for human AWACS controllers to see your callsign.

**Valid examples**
* `DOLT 1-2 | RurouniJones`
* `PHOENIX 7-1 | Jabbawocky`
* `RABBIT 3-82 | Bugs Bunny`
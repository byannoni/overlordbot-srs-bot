﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.IO;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using NLog;

namespace RurouniJones.OverlordBot.Discord.Commands
{
    [Group("overlordbot")]
    internal class StaticCommandsModule : ModuleBase<SocketCommandContext>
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        [Command("help")]
        [Summary("DM OverlordBot instructions to the requester")]
        internal async Task GetHelpAsync()
        {
            try
            {
                var privateChannel = await Context.User.CreateDMChannelAsync();
                await privateChannel.SendMessageAsync(await File.ReadAllTextAsync("Documentation/Callsign Setup.md"));
                await privateChannel.SendMessageAsync(await File.ReadAllTextAsync("Documentation/Voice Training.md"));
                await privateChannel.SendMessageAsync(await File.ReadAllTextAsync("Documentation/AWACS Calls.md"));
                await privateChannel.SendMessageAsync(await File.ReadAllTextAsync("Documentation/ATC Calls.md"));
                await Context.Message.AddReactionAsync(new Emoji("✅"));
            }
            catch (Exception ex)
            {
                _logger.Warn(ex);
                await Context.Message.AddReactionAsync(new Emoji("❎"));
            }
        }

        [Command("discord")]
        [Summary("Post OverlordBot Discord link")]
        internal async Task GetDiscordAsync()
        {
            try
            {
                await ReplyAsync("https://discord.gg/9RqyTJt");
            }
            catch (Exception ex)
            {
                _logger.Warn(ex);
            }
        }
    }
}

﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using RurouniJones.OverlordBot.Encyclopedia;

namespace RurouniJones.OverlordBot.Core.Atc.Util
{
    internal class ActiveRunwayDecider
    {
        /// <summary>
        ///     Some Airfields will have special considerations. For example some will only switch end
        ///     if the wind speed is high enough. For the moment we will stick with wind direction.
        /// </summary>
        /// <param name="airfield"></param>
        /// <returns>A list of active runways</returns>
        public static List<Airfield.Runway> GetActiveRunways(AirfieldStatus airfield)
        {
            var activeRunways = GetActiveRunwaysByWind(airfield);

            if (activeRunways.Count == 0 && airfield.Runways.Count > 0)
                throw new NoActiveRunwaysFoundException(
                    $"Could not find active runways for {airfield.Name} with wind heading {airfield.WindHeading}");
            return activeRunways;
        }

        private static List<Airfield.Runway> GetActiveRunwaysByWind(AirfieldStatus airfield)
        {
            return GetActiveRunwaysByHeading(airfield);
        }

        private static List<Airfield.Runway> GetActiveRunwaysByHeading(AirfieldStatus airfield)
        {
            var activeRunways = new List<Airfield.Runway>();

            // ReSharper disable once ForeachCanBeConvertedToQueryUsingAnotherGetEnumerator
            foreach (var runway in airfield.Runways)
            {
                var wH = airfield.WindHeading == -1 ? 90 : airfield.WindHeading;
                var rH = runway.Heading;

                if (Math.Min(wH - rH < 0 ? wH - rH + 360 : wH - rH, rH - wH < 0 ? rH - wH + 360 : rH - wH) < 90)
                    activeRunways.Add(runway);
            }

            return activeRunways;
        }
    }

    public class NoActiveRunwaysFoundException : Exception
    {
        public NoActiveRunwaysFoundException(string message) : base(message)
        {
        }
    }
}
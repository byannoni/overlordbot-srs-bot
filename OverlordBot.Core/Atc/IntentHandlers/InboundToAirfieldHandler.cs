﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Linq;
using System.Text.RegularExpressions;
using NLog;
using RurouniJones.OverlordBot.Core.Atc.Util;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Core.Util;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Atc.IntentHandlers
{
    internal class InboundToAirfieldHandler
    {
        protected static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static Reply Process(Unit transmitterUnit, AirfieldStatus airfield, Reply reply)
        {
            try
            {
                var approachRoute = airfield.GetApproachRoute(transmitterUnit.Location);

                var initialTrueBearing = transmitterUnit.BearingTo(approachRoute.First());

                var initialMagneticBearing =
                    Regex.Replace(
                        Geospatial.TrueToMagnetic(transmitterUnit.Location, initialTrueBearing).ToString("000"),
                        "\\d{1}", " $0");

                reply.Message = $"fly heading {initialMagneticBearing}, descend and maintain 2,000, reduce speed below 2 5 0 knots, for vectors to {approachRoute.Last().Name}, {approachRoute.First().Name}";

                return reply;
            }
            catch (NoActiveRunwaysFoundException ex)
            {
                Logger.Error(ex, "No Active Runways found");
                reply.Message = "We could not find any active runways.";
                reply.ContinueProcessing = false;
                return reply;
            }
        }
    }
}

﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NLog;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Atc.IntentHandlers;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Atc
{
    public class AtcResponder : BaseResponder
    {
        private static readonly List<string> ValidControllerNames = new() { "ground", "tower", "approach", "center" };
        private const string AtcOnlyMessage = "this is an ATC frequency and we only respond to ATC calls";
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public HashSet<AirfieldStatus> Airfields { get; init; }
        public string ServerShortName { get; init; }

        public AtcResponder(IPlayerRepository playerRepository, IUnitRepository unitRepository, IAirfieldRepository airfieldRepository, HashSet<AirfieldStatus> airfields, string serverShortName) : base(playerRepository, unitRepository, airfieldRepository) {
            Airfields = airfields;
            ServerShortName = serverShortName;
        }

        public override async Task<Reply> ProcessTransmission(ITransmission transmission)
        {
            _logger.Info(transmission);

            var reply = PerformInitialTransmissionChecks(transmission);
            if (reply.ContinueProcessing == false) return reply;

            Unit playerUnit;
            (reply, playerUnit) = await DetermineTransmitter(transmission, reply, ServerShortName);

            var airfield = (ITransmission.Airfield) transmission.Receiver;
            var airfieldName = airfield.Name ?? "ATC";
            reply.ResponderCallsign = string.Join(' ', Cognitive.SpeechOutput.AirbasePronouncer.PronounceAirbase(airfieldName), airfield.Controller);

            if (reply.ContinueProcessing == false) return reply;

            var airfieldStatus = Airfields.FirstOrDefault(x => x.Name.ToLower().Equals(airfield.Name.ToLower()));
            if (airfieldStatus == null || airfieldStatus.Runways.Count == 0 || airfieldStatus.NavigationGraph.EdgeCount == 0)
            {
                if (airfieldStatus == null)
                {
                    reply.Notes = $"{airfield.Name} does not have a mapping file";
                }
                else if (airfieldStatus.Runways.Count == 0)
                {
                    reply.Notes = $"{airfield.Name} does not have any mapped runways";
                }
                else if (airfieldStatus.NavigationGraph.EdgeCount == 0)
                {
                    reply.Notes = $"{airfield.Name} does not have any navigation routes";
                }
                reply.Message = "There are no ATC services currently available at this airfield.";
                reply.ContinueProcessing = false;
            }

            if (reply.ContinueProcessing == false) return reply;

            if (!IsAddressedToAtc(transmission.Receiver))
            {
                var typeName = transmission.Receiver.GetType().Name;
                var article = new List<char> { 'a', 'e', 'i', 'o', 'u' }.Contains(typeName[0]) ? "an" : "a";
                reply.Notes = $"Transmission not addressed to an ATC. It was addressed to {transmission.Receiver.Callsign} which is {article} {typeName}";
                reply.Message = null;

                _logger.Info(reply.Message);
                reply.ContinueProcessing = false;
            }

            if (!ContainsControllerName((ITransmission.Airfield) transmission.Receiver))
            {
                reply.Notes = $"Transmission was not addressed to an airfield controller. ATC transmissions must be <AIRFIELD> followed by one of {string.Join(", ", ValidControllerNames)}. e.g '{airfield.Name} {ValidControllerNames[0]}' ";
                reply.Message = null;

                _logger.Info(reply.Message);
                reply.ContinueProcessing = false;
            }

            if (reply.ContinueProcessing == false) return reply;

            switch (transmission.Intent)
            {
                case ITransmission.Intents.BogeyDope:
                case ITransmission.Intents.LocationOfEntity:
                case ITransmission.Intents.SetWarningRadius:
                case ITransmission.Intents.Declare:
                case ITransmission.Intents.Picture:
                    reply.Message = AtcOnlyMessage;
                    break;
                case ITransmission.Intents.AbortInbound:
                    reply.Message = "Copy abort, call inbound again when ready";
                    break;

                case ITransmission.Intents.RadioCheck:
                    reply.Message = "5 by 5";
                    break;
                case ITransmission.Intents.ReadyToTaxi:
                    reply = ReadyToTaxiHandler.Process(playerUnit, airfieldStatus, reply);
                    break;
                case ITransmission.Intents.InboundToAirfield:
                    reply = InboundToAirfieldHandler.Process(playerUnit, airfieldStatus, reply);
                    break;
                default:
                    _logger.Warn(
                        $"Unknown Intent: {transmission.Intent}, Transmitter: {transmission.Transmitter}, Receiver: {transmission.Receiver}");
                    break;
            }
            return reply;
        }

        private static bool IsAddressedToAtc(ITransmission.IReceiver receiver)
        {
            return receiver is ITransmission.Airfield;
        }

        private static bool ContainsControllerName(ITransmission.Airfield airfield)
        {
            return ValidControllerNames.Contains(airfield.Controller?.ToLower());

        }
    }
}
﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Cognitive.SpeechRecognition;
using RurouniJones.OverlordBot.Core.Responders;

namespace RurouniJones.OverlordBot.Core.Models
{
    public class RequestResponseModel
    {
        public TransmissionRecognizer.Result RecognitionResult { get; set; }
        public ITransmission Transmission { get; set; }
        public Exception TransmissionException { get; set; }

        public Reply Reply { get; set; }

        public long ProcessingTime { get; set; }
        
        public RequestResponseModel()
        {
            Transmission = null;
            TransmissionException = null;
            Reply = null;
        }
    }
}

﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Text;
using DEM.Net.Core;
using DEM.Net.Core.Datasets;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using NLog;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Util
{
    public class LineOfSightChecker
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly DEMDataSet DataSet = DEMDataSet.SRTM_GL1;
        private static readonly ElevationService ElevationService = InitializeElevationService();

        private static IDEMDataSetIndex RasterIndexServiceResolver(DEMDataSourceType dataSourceType)
        {
            return new GDALVRTFileService(null, new Logger<GDALVRTFileService>(new NullLoggerFactory()));
        }

        private static ElevationService InitializeElevationService()
        {
            var rasterService = new RasterService(RasterIndexServiceResolver, new Logger<RasterService>(new NullLoggerFactory()));
            var elevationService = new ElevationService(rasterService, new Logger<ElevationService>(new NullLoggerFactory()));
            var map = new BoundingBox(37, 45, 41, 45);
            elevationService.DownloadMissingFiles(DataSet, map);
            return elevationService;
        }

        public static bool HasLineOfSight(Unit source, Unit target)
        {
            var sourcePoint = new GeoPoint(source.Location.Coordinate.Latitude, source.Location.Coordinate.Longitude);
            var targetPoint = new GeoPoint(target.Location.Coordinate.Latitude, target.Location.Coordinate.Longitude);

            var elevationLine = GeometryService.ParseGeoPointAsGeometryLine(sourcePoint, targetPoint);
            var geoPoints = ElevationService.GetLineGeometryElevation(elevationLine, DataSet);

            geoPoints[0].Elevation = source.Altitude;
            geoPoints[^1].Elevation = target.Altitude;

            var metrics = geoPoints.ComputeVisibilityMetrics(0d, 0d, DataSet.NoDataValue);
            var report = new IntervisibilityReport(geoPoints, metrics, originVerticalOffset: 0d, targetVerticalOffset: 0d);

            if (!Logger.IsTraceEnabled) return !report.HasObstacles;
            var sb = new StringBuilder();
            sb.AppendLine("LOS Report");
            sb.AppendLine(
                $"Source: {source.Name} '{source.Pilot}', Lat/Lon: {sourcePoint.Latitude}/{sourcePoint.Longitude} at {source.Altitude} meters");
            sb.AppendLine(
                $"Target: {target.Name} '{target.Pilot}', Lat/Lon: {targetPoint.Latitude}/{targetPoint.Longitude} at {target.Altitude} meters");
            sb.AppendLine("Report:");
            sb.AppendLine(
                $"  Origin: Lat/Lon {report.Origin.Latitude}/{report.Origin.Longitude}. elevation {report.Origin.Elevation}, vertical offset {report.OriginVerticalOffset}");
            sb.AppendLine(
                $"  Target: Lat/Lon {report.Target.Latitude}/{report.Target.Longitude}. elevation {report.Target.Elevation}, vertical offset {report.TargetVerticalOffset}");
            sb.AppendLine($"  Obstructed: {report.HasObstacles}");
            sb.AppendLine($"  Obstacles count: {report.ObstacleCount} of {report.GeoPoints.Count} points");
            sb.AppendLine($"  Metrics: {report.Metrics}");
            /*
            sb.AppendLine($"  Points:");
            foreach (var point in report.GeoPoints)
            {
                sb.AppendLine(
                    $"    Lat/Lon {point.Latitude}/{point.Longitude} at {point.Elevation} meters. Distance from Origin {point.DistanceFromOriginMeters}");
            }*/

            sb.AppendLine($"  Obstacles:");
            foreach (var obstacle in report.Metrics.Obstacles)
            {
                sb.AppendLine(
                    $"    Lat/Lon {obstacle.PeakPoint.Latitude}/{obstacle.PeakPoint.Longitude} at {obstacle.PeakPoint.Elevation} meters");
            }

            Logger.Trace(sb.ToString);
            return !report.HasObstacles;
        }

    }
}

﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using Microsoft.Azure.CognitiveServices.Language.LUIS.Runtime.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;

namespace RurouniJones.OverlordBot.Cognitive.Tests.TransmissionParser.TransmitterParsing
{
    [TestClass]
    public class PlayerParsingTests
    {
        [TestMethod]
        public void  WhenRequestEntityIsMissing_SetsTransmitterToUnknownEntity()
        {
            const string json = @"
            {
	            ""query"": ""radio check"",
	            ""prediction"": {
		            ""alteredQuery"": null,
		            ""topIntent"": ""radioCheck"",
		            ""intents"": {
			            ""radioCheck"": {
				            ""score"": 1.0,
				            ""childApp"": null
			            }
		            },
		            ""entities"": {
		            },
		            ""sentiment"": null
	            }
            }";

            var prediction = JsonConvert.DeserializeObject<PredictionResponse>(json);
            var transmission = LanguageUnderstanding.TransmissionParser.ParseResponse(prediction);
            var radioCheckTransmission = (BasicTransmission)transmission;

            Assert.IsInstanceOfType(radioCheckTransmission.Transmitter, typeof(ITransmission.UnknownEntity));
        }

        [TestMethod]
        public void  WhenTransmitterEntityIsMissing_SetsTransmitterToUnknownEntity()
        {
            const string json = @"
            {
	            ""query"": ""radio check"",
	            ""prediction"": {
		            ""alteredQuery"": null,
		            ""topIntent"": ""radioCheck"",
		            ""intents"": {
			            ""radioCheck"": {
				            ""score"": 1.0,
				            ""childApp"": null
			            }
		            },
		            ""entities"": {},
		            ""sentiment"": null
	            }
            }";

            var prediction = JsonConvert.DeserializeObject<PredictionResponse>(json);
            var transmission = LanguageUnderstanding.TransmissionParser.ParseResponse(prediction);
            var radioCheckTransmission = (BasicTransmission)transmission;

            Assert.IsInstanceOfType(radioCheckTransmission.Transmitter, typeof(ITransmission.UnknownEntity));
        }

        [TestMethod]
        public void  WhenPlayerEntityIsMissing_SetsTransmitterToUnknownEntity()
        {
            const string json = @"
            {
	            ""query"": ""radio check"",
	            ""prediction"": {
		            ""alteredQuery"": null,
		            ""topIntent"": ""radioCheck"",
		            ""intents"": {
			            ""radioCheck"": {
				            ""score"": 1.0,
				            ""childApp"": null
			            }
		            },
		            ""entities"": {
			            ""radioCheckRequest"": [{
				            ""transmitter"": []
			            }]
		            },
		            ""sentiment"": null
	            }
            }";

            var prediction = JsonConvert.DeserializeObject<PredictionResponse>(json);
            var transmission = LanguageUnderstanding.TransmissionParser.ParseResponse(prediction);
            var radioCheckTransmission = (BasicTransmission)transmission;

            Assert.IsInstanceOfType(radioCheckTransmission.Transmitter, typeof(ITransmission.UnknownEntity));
        }

        [TestMethod]
        public void  WhenPlayerEntityIsCorrectWithSeparateFlightAndElement_SetsTransmitterToPlayer()
        {
            const string json = @"
            {
	            ""query"": ""Dolt 1 2 radio check"",
	            ""prediction"": {
		            ""alteredQuery"": null,
		            ""topIntent"": ""radioCheck"",
		            ""intents"": {
			            ""radioCheck"": {
				            ""score"": 1.0,
				            ""childApp"": null
			            }
		            },
		            ""entities"": {
			            ""radioCheckRequest"": [{
				            ""transmitter"": [{
					            ""player"": [{
						            ""groupName"": [""Dolt""],
						            ""flight"": [""1""],
						            ""element"": [""2""]
					            }]
				            }]
			            }]
		            },
		            ""sentiment"": null
	            }
            }";

            var prediction = JsonConvert.DeserializeObject<PredictionResponse>(json);
            var transmission = LanguageUnderstanding.TransmissionParser.ParseResponse(prediction);
            var radioCheckTransmission = (BasicTransmission)transmission;

            Assert.IsInstanceOfType(radioCheckTransmission.Transmitter, typeof(ITransmission.Player));

            var expectedPlayer = new ITransmission.Player("Dolt", 1, 2);
            var actualPlayer = (ITransmission.Player) radioCheckTransmission.Transmitter;

            Assert.AreEqual(expectedPlayer, actualPlayer);
        }

        [TestMethod]
        public void  WhenPlayerEntityIsCorrectWithCombinedFlightAndElement_SetsTransmitterToPlayer()
        {
            const string json = @"
            {
	            ""query"": ""Dolt 1 2 radio check"",
	            ""prediction"": {
		            ""alteredQuery"": null,
		            ""topIntent"": ""radioCheck"",
		            ""intents"": {
			            ""radioCheck"": {
				            ""score"": 1.0,
				            ""childApp"": null
			            }
		            },
		            ""entities"": {
			            ""radioCheckRequest"": [{
				            ""transmitter"": [{
					            ""player"": [{
						            ""groupName"": [""Dolt""],
						            ""flightAndElement"": [""12""]
					            }]
				            }]
			            }]
		            },
		            ""sentiment"": null
	            }
            }";

            var prediction = JsonConvert.DeserializeObject<PredictionResponse>(json);
            var transmission = LanguageUnderstanding.TransmissionParser.ParseResponse(prediction);
            var radioCheckTransmission = (BasicTransmission)transmission;

            Assert.IsInstanceOfType(radioCheckTransmission.Transmitter, typeof(ITransmission.Player));

            var expectedPlayer = new ITransmission.Player("Dolt", 1, 2);
            var actualPlayer = (ITransmission.Player) radioCheckTransmission.Transmitter;

            Assert.AreEqual(expectedPlayer, actualPlayer);
        }

        [TestMethod]
        public void  WhenPlayerCallsignIsRecognizedAsTwoWords_SetsTransmitterToPlayerWithConcatenatedCallsign()
        {
            const string json = @"
            {
	            ""query"": ""war dog 1 2 radio check"",
	            ""prediction"": {
		            ""alteredQuery"": null,
		            ""topIntent"": ""radioCheck"",
		            ""intents"": {
			            ""radioCheck"": {
				            ""score"": 1.0,
				            ""childApp"": null
			            }
		            },
		            ""entities"": {
			            ""radioCheckRequest"": [{
				            ""transmitter"": [{
					            ""player"": [{
						            ""groupName"": [""war dog""],
						            ""flightAndElement"": [""12""]
					            }]
				            }]
			            }]
		            },
		            ""sentiment"": null
	            }
            }";

            var prediction = JsonConvert.DeserializeObject<PredictionResponse>(json);
            var transmission = LanguageUnderstanding.TransmissionParser.ParseResponse(prediction);
            var radioCheckTransmission = (BasicTransmission)transmission;

            Assert.IsInstanceOfType(radioCheckTransmission.Transmitter, typeof(ITransmission.Player));

            var expectedPlayer = new ITransmission.Player("wardog", 1, 2);
            var actualPlayer = (ITransmission.Player) radioCheckTransmission.Transmitter;

            Assert.AreEqual(expectedPlayer, actualPlayer);
        }

        [TestMethod]
        public void  WhenGroupNameEntityIsMissing_SetsTransmitterToPlayer()
        {
            const string json = @"
            {
	            ""query"": ""1 2 radio check"",
	            ""prediction"": {
		            ""alteredQuery"": null,
		            ""topIntent"": ""radioCheck"",
		            ""intents"": {
			            ""radioCheck"": {
				            ""score"": 1.0,
				            ""childApp"": null
			            }
		            },
		            ""entities"": {
			            ""radioCheckRequest"": [{
				            ""transmitter"": [{
					            ""player"": [{
						            ""flight"": [""1""],
						            ""element"": [""2""]
					            }]
				            }]
			            }]
		            },
		            ""sentiment"": null
	            }
            }";

            var prediction = JsonConvert.DeserializeObject<PredictionResponse>(json);
            var transmission = LanguageUnderstanding.TransmissionParser.ParseResponse(prediction);
            var radioCheckTransmission = (BasicTransmission)transmission;

            Assert.IsInstanceOfType(radioCheckTransmission.Transmitter, typeof(ITransmission.Player));

            var expectedPlayer = new ITransmission.Player(null, 1, 2);
            var actualPlayer = (ITransmission.Player) radioCheckTransmission.Transmitter;

            Assert.AreEqual(expectedPlayer, actualPlayer);
        }

        [TestMethod]
        public void  WhenFlightEntityIsMissing_SetsTransmitterToPlayer()
        {
            const string json = @"
            {
	            ""query"": ""Dolt 1 2 radio check"",
	            ""prediction"": {
		            ""alteredQuery"": null,
		            ""topIntent"": ""radioCheck"",
		            ""intents"": {
			            ""radioCheck"": {
				            ""score"": 1.0,
				            ""childApp"": null
			            }
		            },
		            ""entities"": {
			            ""radioCheckRequest"": [{
				            ""transmitter"": [{
					            ""player"": [{
						            ""groupName"": [""Dolt""],
						            ""element"": [""2""]
					            }]
				            }]
			            }]
		            },
		            ""sentiment"": null
	            }
            }";

            var prediction = JsonConvert.DeserializeObject<PredictionResponse>(json);
            var transmission = LanguageUnderstanding.TransmissionParser.ParseResponse(prediction);
            var radioCheckTransmission = (BasicTransmission)transmission;

            Assert.IsInstanceOfType(radioCheckTransmission.Transmitter, typeof(ITransmission.Player));

            var expectedPlayer = new ITransmission.Player("Dolt", -1, 2);
            var actualPlayer = (ITransmission.Player) radioCheckTransmission.Transmitter;

            Assert.AreEqual(expectedPlayer, actualPlayer);
        }

        [TestMethod]
        public void  WhenElementEntityIsMissing_SetsTransmitterToPlayer()
        {
            const string json = @"
            {
	            ""query"": ""Dolt 1 2 radio check"",
	            ""prediction"": {
		            ""alteredQuery"": null,
		            ""topIntent"": ""radioCheck"",
		            ""intents"": {
			            ""radioCheck"": {
				            ""score"": 1.0,
				            ""childApp"": null
			            }
		            },
		            ""entities"": {
			            ""radioCheckRequest"": [{
				            ""transmitter"": [{
					            ""player"": [{
						            ""groupName"": [""Dolt""],
						            ""flight"": [""1""]
					            }]
				            }]
			            }]
		            },
		            ""sentiment"": null
	            }
            }";

            var prediction = JsonConvert.DeserializeObject<PredictionResponse>(json);
            var transmission = LanguageUnderstanding.TransmissionParser.ParseResponse(prediction);
            var radioCheckTransmission = (BasicTransmission)transmission;

            Assert.IsInstanceOfType(radioCheckTransmission.Transmitter, typeof(ITransmission.Player));

            var expectedPlayer = new ITransmission.Player("Dolt", 1, -1);
            var actualPlayer = (ITransmission.Player) radioCheckTransmission.Transmitter;

            Assert.AreEqual(expectedPlayer, actualPlayer);
        }

        [TestMethod]
        public void  WhenOnlyGroupNameEntityIsPresent_SetsTransmitterToPlayer()
        {
            const string json = @"
            {
	            ""query"": ""Dolt 1 2 radio check"",
	            ""prediction"": {
		            ""alteredQuery"": null,
		            ""topIntent"": ""radioCheck"",
		            ""intents"": {
			            ""radioCheck"": {
				            ""score"": 1.0,
				            ""childApp"": null
			            }
		            },
		            ""entities"": {
			            ""radioCheckRequest"": [{
				            ""transmitter"": [{
					            ""player"": [{
						            ""groupName"": [""Dolt""]
					            }]
				            }]
			            }]
		            },
		            ""sentiment"": null
	            }
            }";

            var prediction = JsonConvert.DeserializeObject<PredictionResponse>(json);
            var transmission = LanguageUnderstanding.TransmissionParser.ParseResponse(prediction);
            var radioCheckTransmission = (BasicTransmission)transmission;

            Assert.IsInstanceOfType(radioCheckTransmission.Transmitter, typeof(ITransmission.Player));

            var expectedPlayer = new ITransmission.Player("Dolt", -1, -1);
            var actualPlayer = (ITransmission.Player) radioCheckTransmission.Transmitter;

            Assert.AreEqual(expectedPlayer, actualPlayer);
        }
    }
}

﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using Microsoft.Azure.CognitiveServices.Language.LUIS.Runtime.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;

namespace RurouniJones.OverlordBot.Cognitive.Tests.TransmissionParser.ReceiverParsing
{
    [TestClass]
    public class AwacsParsingTests
    {
        [TestMethod]
        public void  WhenRequestEntityIsMissing_SetsReceiverToUnknownEntity()
        {
            const string json = @"
            {
	            ""query"": ""radio check"",
	            ""prediction"": {
		            ""alteredQuery"": null,
		            ""topIntent"": ""radioCheck"",
		            ""intents"": {
			            ""radioCheck"": {
				            ""score"": 1.0,
				            ""childApp"": null
			            }
		            },
		            ""entities"": {
		            },
		            ""sentiment"": null
	            }
            }";

            var prediction = JsonConvert.DeserializeObject<PredictionResponse>(json);
            var transmission = LanguageUnderstanding.TransmissionParser.ParseResponse(prediction);
            var radioCheckTransmission = (BasicTransmission)transmission;

            Assert.IsInstanceOfType(radioCheckTransmission.Receiver, typeof(ITransmission.UnknownEntity));
        }

        [TestMethod]
        public void  WhenReceiverEntityIsMissing_SetsReceiverToUnknownEntity()
        {
            const string json = @"
            {
	            ""query"": ""radio check"",
	            ""prediction"": {
		            ""alteredQuery"": null,
		            ""topIntent"": ""radioCheck"",
		            ""intents"": {
			            ""radioCheck"": {
				            ""score"": 1.0,
				            ""childApp"": null
			            }
		            },
		            ""entities"": {},
		            ""sentiment"": null
	            }
            }";

            var prediction = JsonConvert.DeserializeObject<PredictionResponse>(json);
            var transmission = LanguageUnderstanding.TransmissionParser.ParseResponse(prediction);
            var radioCheckTransmission = (BasicTransmission)transmission;

            Assert.IsInstanceOfType(radioCheckTransmission.Receiver, typeof(ITransmission.UnknownEntity));
        }

        [TestMethod]
        public void  WhenAwacsEntityIsMissing_SetsReceiverToUnknownEntity()
        {
            const string json = @"
            {
	            ""query"": ""radio check"",
	            ""prediction"": {
		            ""alteredQuery"": null,
		            ""topIntent"": ""radioCheck"",
		            ""intents"": {
			            ""radioCheck"": {
				            ""score"": 1.0,
				            ""childApp"": null
			            }
		            },
		            ""entities"": {
			            ""radioCheckRequest"": [{
				            ""receiver"": []
			            }]
		            },
		            ""sentiment"": null
	            }
            }";

            var prediction = JsonConvert.DeserializeObject<PredictionResponse>(json);
            var transmission = LanguageUnderstanding.TransmissionParser.ParseResponse(prediction);
            var radioCheckTransmission = (BasicTransmission)transmission;

            Assert.IsInstanceOfType(radioCheckTransmission.Receiver, typeof(ITransmission.UnknownEntity));
        }

        [TestMethod]
        public void  WhenAwacsEntityIsCorrect_SetsReceiverToAwacs()
        {
            const string json = @"
            {
	            ""query"": ""overlord radio check"",
	            ""prediction"": {
		            ""alteredQuery"": null,
		            ""topIntent"": ""radioCheck"",
		            ""intents"": {
			            ""radioCheck"": {
				            ""score"": 1.0,
				            ""childApp"": null
			            }
		            },
		            ""entities"": {
			            ""radioCheckRequest"": [{
				            ""receiver"": [{
					            ""awacs"": [""overlord""]
				            }]
			            }]
		            },
		            ""sentiment"": null
	            }
            }";

            var prediction = JsonConvert.DeserializeObject<PredictionResponse>(json);
            var transmission = LanguageUnderstanding.TransmissionParser.ParseResponse(prediction);
            var radioCheckTransmission = (BasicTransmission)transmission;

            Assert.IsInstanceOfType(radioCheckTransmission.Receiver, typeof(ITransmission.Awacs));

            var expectedAwacs = new ITransmission.Awacs("overlord");
            var actualAwacs = (ITransmission.Awacs) radioCheckTransmission.Receiver;

            Assert.AreEqual(expectedAwacs, actualAwacs);
        }
    }
}

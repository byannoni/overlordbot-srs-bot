#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/runtime:5.0 AS base
RUN apt-get update
RUN apt-get -y install build-essential libasound2 wget
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["OverlordBot.Console/OverlordBot.Console.csproj", "OverlordBot.Console/"]
COPY ["OverlordBot.Core/OverlordBot.Core.csproj", "OverlordBot.Core/"]
COPY ["OverlordBot.Diagnostics/OverlordBot.Diagnostics.csproj", "OverlordBot.Diagnostics/"]
COPY ["OverlordBot.Discord/OverlordBot.Discord.csproj", "OverlordBot.Discord/"]
COPY ["OverlordBot.Administration/OverlordBot.Administration.csproj", "OverlordBot.Administration/"]
COPY ["OverlordBot.SimpleRadio/OverlordBot.SimpleRadio.csproj", "OverlordBot.SimpleRadio/"]
COPY ["OverlordBot.Cognitive/OverlordBot.Cognitive.csproj", "OverlordBot.Cognitive/"]
COPY ["OverlordBot.Encyclopedia/OverlordBot.Encyclopedia.csproj", "OverlordBot.Encyclopedia/"]
COPY ["OverlordBot.Datastore/OverlordBot.Datastore.csproj", "OverlordBot.Datastore/"]
RUN dotnet restore "OverlordBot.Console/OverlordBot.Console.csproj"
COPY . .
WORKDIR "/src/OverlordBot.Console"
RUN dotnet build "OverlordBot.Console.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "OverlordBot.Console.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "OverlordBot.Console.dll"]
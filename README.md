# OverlordBot

## Introduction

OverlordBot is An AWACS Voice bot for Digital Combat Simulator written by
RurouniJones for use with SimpleRadio Standalone (SRS) written by Ciribob.

Please use the following resources to learn more and get in touch.

* [Player Guide](https://gitlab.com/overlord-bot/srs-bot/-/wikis/Interaction)
* [Youtube](https://www.youtube.com/channel/UCFDs2Rr9CGOZhVJITT6anCQ)
* [Discord](https://discord.gg/9RqyTJt)
* [Project](https://gitlab.com/overlord-bot)
